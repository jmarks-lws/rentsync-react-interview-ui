
export interface IClient {
  id: number,
  name: string,
  email: string,
  phone?: string,
  address: string,
  city: string,
  province: string,
  postal: string,
  country: string,
}

export interface ILine {
  item: string,
  description: string,
  unitCost: number,
  quantity: number,
  order: number,
}

export interface IInvoice {
  id: number,
  invoiceNumber: string,
  invoiceDate: string,
  dueDate: string,
  amountPaid: number,
  status: string,
  client: IClient,
  lines: ILine[],
}

export type TRole = 'DEVELOPER' | 'ADMIN' | 'USER';

export interface IUser {
  id: number,
  email: string,
  password: string,
  name: string,
  roles: TRole[],
  token: string,
}

export type TCollection = 'users' | 'clients' | 'invoices';

export type TDataFile = {
  clients: IClient[],
  invoices: IInvoice[],
  users: IUser[],
};

export const loadDatabase = async () => (await import(`../data/data.json`)).default;

export const loadClientData = async () => (await loadDatabase()).clients;
export const loadInvoiceData = async () => (await loadDatabase()).invoices;
export const loadUserData = async () => (await loadDatabase()).users;
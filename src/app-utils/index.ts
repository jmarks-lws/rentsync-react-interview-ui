import { intVal } from "@jamesgmarks/utilities";

/**
 *
 * @param year A four digit year.
 * @param month A month number between 1-12.
 */
export const getMonthStart = (year: number, month: number) => new Date(year, month - 1, 1);
/**
  *
  * @param year A four digit year.
  * @param month A month number between 1-12.
  */
export const getMonthEnd = (year: number, month: number, endOfDay: boolean = true) => (
  endOfDay
    ? new Date(year, month, 0, 23, 59, 59, 999)
    : new Date(year, month, 0)
);

export const getLastDayOfThisMonth = () => {
  const today = new Date();
  return getMonthEnd(today.getFullYear(), today.getMonth() + 1, false);
}

const usdFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',

  // These options are needed to round to whole numbers if that's what you want.
  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});

export const roundFloatToCentsAsNumber = (float: number, algorithm: 'up' | 'down' | 'nearest' = 'nearest') => {
  // eslint-disable-next-line no-nested-ternary
  const rounder = algorithm === 'nearest' ? Math.round : (algorithm === 'up' ? Math.ceil : Math.floor);
  const nearestPennyUp = `${rounder(float * 100)}`.padStart(2, '0');

  return parseFloat(`${nearestPennyUp.slice(0, -2)}.${nearestPennyUp.slice(-2)}`);
};

export const roundFloatToCents = (float: number, algorithm: 'up' | 'down' | 'nearest' = 'nearest') => {
  const absFloat = Math.abs(float);
  const isNegativeMultiplier = float < 0 ? -1 : 1;
  // eslint-disable-next-line no-nested-ternary
  const rounder = algorithm === 'nearest' ? Math.round : (algorithm === 'up' ? Math.ceil : Math.floor);
  const nearestPennyUp = `${rounder(absFloat * 100)}`.padStart(2, '0');

  return (
    usdFormatter
      .format(parseFloat(`${nearestPennyUp.slice(0, -2)}.${nearestPennyUp.slice(-2)}`) * isNegativeMultiplier)
  );
};

export const toDollarAmount = (
  num: number,
  negativeStrategy: 'brackets' | 'symbolPrefix' = 'symbolPrefix',
  fractionalDigits: number = 2,
) => {
  const applyNegativeStrategy = (number: string) => {
    const isNegative = number.startsWith('$-');
    return (isNegative && negativeStrategy === 'brackets')
      ? `($${number.slice(2)})`
      : `${isNegative ? '-$' : '$'}${number.slice(isNegative ? 2 : 1)}`
  }
  const formattedNum = applyNegativeStrategy('$' + num.toFixed(fractionalDigits).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
  return formattedNum;
};

export const dollarToFloat = (currency: string) => Number(currency.replace(/[^0-9.-]+/g, ""));

export const ucaseFirst = (str: string) => (`${str.slice(0, 1).toUpperCase()}${str.slice(1).toLowerCase()}`);

const monthNames = [
  'January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December',
];
export const getMonthName = (date: Date) => monthNames[date.getMonth()];

export const isDST = (date: Date) => {
  let jan = new Date(date.getFullYear(), 0, 1).getTimezoneOffset();
  let jul = new Date(date.getFullYear(), 6, 1).getTimezoneOffset();
  return Math.max(jan, jul) !== date.getTimezoneOffset();
}

export const addDays = (date: Date, days: number) => {
  const addedDate = new Date(date);
  addedDate.setDate(addedDate.getDate() + days);
  return addedDate;
};

export const makeYmd = (date: Date, zeroPad: boolean = false): string => {
  const _month = `${date.getMonth() + 1}`.padStart(zeroPad ? 2 : 1, '0');
  const _date = `${date.getDate()}`.padStart(zeroPad ? 2 : 1, '0');
  return `${date.getFullYear()}-${_month}-${_date}`;
};

export const getNumDaysBetween = (
  startDate: Date,
  endDate: Date,
): number => Math.abs(Math.round((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24)));

//Math.round((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
//Math.abs(Math.ceil((endDate.getTime() - startDate.getTime()) / (1000 * 3600 * 24)));

const isPayDate = (currentDate: Date) => {
  const firstPayDate = new Date(2010, 8, 15);
  const difference = getNumDaysBetween(firstPayDate, currentDate);

  return ((difference % 14) === 0);
};

// Recursive method to find next pay day
export const getNextPayDate = (today: Date = new Date(new Date().setHours(0, 0, 0, 0)), days = 0): Date => {
  const addedDate = addDays(today, days);
  return isPayDate(addedDate) ? addedDate : getNextPayDate(today, days + 1);
};

export const dateFromYmd = (ymdDate: string): Date => {
  // TODO: Sensible validator for YMD string.
  const [year, month, date] = (ymdDate ?? '').split('-');
  const today = new Date();
  return new Date(
    year ? intVal(year) : today.getFullYear(),
    month ? intVal(month) - 1 : today.getMonth(),
    date ? intVal(date) : today.getDate(),
  );
};
export const dateFromYmdHms = (ymdHmsDate: string): Date => {
  const [ymdDate, hms] = ymdHmsDate.split(' ');
  const [year, month, date] = ymdDate.split('-').map(intVal);
  const [hours, minutes, seconds] = (hms ?? '0:00:00').split(':').map(intVal);
  return new Date(year, month - 1, date, hours, minutes, seconds);
};

export const flattenOneLevel = <T>(usageOrCostArray: Array<Array<T>>): T[] => (
  usageOrCostArray.reduce((acc, curr) => [ ...acc, ...curr ], [])
);
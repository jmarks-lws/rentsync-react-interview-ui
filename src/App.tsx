import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';

import { Navbar } from './components/parts/Navbar';

import { Home } from './components/pages/Home';
import { ClientsContainer } from './components/pages/Clients/Clients';
import { InvoicesContainer } from './components/pages/Invoices/InvoicesContainer';

import './App.css';

function App() {
  return (
    <Router>
      <Navbar isLoggedIn={true} />
      <Switch>
        <Route path="/clients"><ClientsContainer /></Route>
        <Route path="/invoices"><InvoicesContainer /></Route>
        <Route path="/"><Home /></Route>
      </Switch>
    </Router>
  );
}

export default App;

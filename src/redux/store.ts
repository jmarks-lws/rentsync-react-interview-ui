import { configureStore } from '@reduxjs/toolkit'

import clientsSlice from './features/clients/clientsSlice';
import invoicesSlice from './features/invoices/invoiceSlice';

export const store = configureStore({
  reducer: {
    clients: clientsSlice,
    invoices: invoicesSlice,
  },
})

export const dispatch = store.dispatch;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
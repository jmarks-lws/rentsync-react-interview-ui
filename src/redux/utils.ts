export enum ELoadState {
  /** Nothing happening */
  idle = 'idle',
  /** Waiting for response from a remote server. */
  pending = 'pending',
  /** Response received, not yet 'loaded' */
  loading = 'loading',
  /** Error occurred */
  error = 'error',
  /** Load is completed. */
  loaded = 'loaded',
}

export interface IActionType<T> {
  /** String identifier for action type */
  type: string,
  /** Payload/data included with action */
  payload: T,
}

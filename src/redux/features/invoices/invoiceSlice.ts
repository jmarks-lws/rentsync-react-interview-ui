import { createSlice } from '@reduxjs/toolkit';
import { Nullable } from '@jamesgmarks/utilities';
import { IActionType } from '../../utils';
import { IInvoice } from '../../../app-utils/database';

export interface IInvoicesState {
  loadedState: string,
  invoices: IInvoice[],
  currentInvoice: Nullable<IInvoice>,
};

export const invoiceSlice = createSlice({
  name: 'invoices',
  initialState: {
    loadedState: 'idle',
    invoices: [],
    currentInvoice: null,
  } as IInvoicesState,
  reducers: {
    invoicesListReceived: (state, action: IActionType<IInvoice[]>) => {
      console.log(`Receiving (${action.payload?.length}) invoices.`);
      state.invoices = action.payload;
      state.loadedState = 'loaded';
    },
    currentInvoiceReceived: (state, action: IActionType<IInvoice | null>) => {
      state.currentInvoice = action.payload;
      state.loadedState = 'loaded';
    },
  },
})

// Action creators are generated for each case reducer function
export const {
  invoicesListReceived,
  currentInvoiceReceived,
} = invoiceSlice.actions

export default invoiceSlice.reducer
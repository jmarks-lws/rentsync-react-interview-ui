import { store, dispatch } from '../../store';
import { loadInvoiceData } from '../../../app-utils/database';
import {
  invoicesListReceived,
  currentInvoiceReceived,
} from './invoiceSlice';

export const loadInvoices = async () => {
  const invoices = await loadInvoiceData();
  dispatch(invoicesListReceived(invoices));
}

export const setCurrentInvoice = (
  invoiceId: number | null,
) => {
  if (invoiceId == null) dispatch(currentInvoiceReceived(null));
  const invoice = store.getState().invoices.invoices.find(
    (invoice) => invoice.id === invoiceId,
  );
  if (invoice) {
    dispatch(currentInvoiceReceived(invoice));
  }
}
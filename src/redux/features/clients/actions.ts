import { store, dispatch } from '../../store';
import { loadClientData } from '../../../app-utils/database';
import {
  clientsListReceived,
  currentClientReceived,
} from './clientsSlice';

export const loadClients = async () => {
  const clients = await loadClientData();
  dispatch(clientsListReceived(clients));
}

export const setCurrentClient = (
  clientId: number,
) => {
  const client = store.getState().clients.clients.find(
    (client) => client.id === clientId,
  );
  if (client) {
    dispatch(currentClientReceived(client));
  }
}
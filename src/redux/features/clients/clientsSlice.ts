import { createSlice } from '@reduxjs/toolkit';
import { Nullable } from '@jamesgmarks/utilities';
import { IActionType } from '../../utils';
import { IClient } from '../../../app-utils/database';

export interface IClientsState {
  loadedState: string,
  clients: IClient[],
  currentClient: Nullable<IClient>,
};

export const clientsSlice = createSlice({
  name: 'clients',
  initialState: {
    loadedState: 'idle',
    clients: [],
    currentClient: null,
  } as IClientsState,
  reducers: {
    clientsListReceived: (state, action: IActionType<IClient[]>) => {
      state.clients = action.payload;
      state.loadedState = 'loaded';
    },
    currentClientReceived: (state, action: IActionType<IClient>) => {
      state.currentClient = (action.payload ?? null);
    },
  },
})

export const {
  clientsListReceived,
  currentClientReceived,
} = clientsSlice.actions

export default clientsSlice.reducer
import { isNumeric } from '@jamesgmarks/utilities';

import { useCallback, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Container, Table, Row, Col, Card } from 'react-bootstrap';

import { roundFloatToCentsAsNumber } from '../../../app-utils';
import { ILine } from '../../../app-utils/database';

import { useAppSelector } from '../../../redux/hooks';
import { setCurrentInvoice } from '../../../redux/features/invoices/actions';

export const InvoiceLine = ({ line } : { line: ILine }) => {
  return (
    <tr>
      <td>{line.item}</td>
      <td>{line.description}</td>
      <td>{line.unitCost}</td>
      <td>{line.quantity}</td>
      <td>{line.quantity * line.unitCost}</td>
    </tr>
  )
}

export const InvoiceSingleView = () => {
  const invoice = useAppSelector((state) => state.invoices.currentInvoice);

  const { invoiceId } = useParams<{ invoiceId?: string }>();

  // State used to manage line editing
  const [ lineData, setLineData ] = useState(invoice?.lines ?? []);

  useEffect(
    () => { setCurrentInvoice(invoiceId && isNumeric(invoiceId) ? parseInt(invoiceId, 10) : null); },
    [ invoiceId ],
  );

  const getSortedInvoiceLines = useCallback(() => {
    return (invoice?.lines ?? []).slice().sort((a, b) => a.order - b.order);
  }, [ invoice?.lines ]);

  useEffect(() => {
    if (invoice) { setLineData(getSortedInvoiceLines()); }
  }, [ invoice, getSortedInvoiceLines ]);

  const invoiceAddress = (
    `${invoice?.client?.address}`
    + `${invoice?.client?.city}, ${invoice?.client?.province}`
    + `${invoice?.client?.postal}`
  );

  // Original subtotal before any inline edits.
  const invoiceSubtotal = roundFloatToCentsAsNumber(
    (invoice?.lines || []).reduce((acc: number, line) => acc + (line.quantity * line.unitCost), 0),
  );

  const invoiceTaxRate = '0%';
  const invoiceTax = '$0';
  const invoiceTotal = '$0';
  const invoicePaidAmount = '$0';
  const outstandingAmount = '$0';

  return (
    <>
      <Container>
        <Row><Col>&nbsp;</Col></Row>
        <Row className="align-items-start">
          <Col>
            <div>
              <h4>
                <strong>Invoice #: </strong>
                <span
                  title={`Rentsync ID: ${invoice?.id}}`}
                >{invoice?.invoiceNumber ?? 'Not published'}</span>
              </h4>
            </div><div>
              <strong>Client: </strong>
              <Link
                to={`/clients/${invoice?.client.id}`}
              >
                {invoice?.client.name}
              </Link>
            </div>
            <div>
              <strong>Status: </strong>
              {invoice?.status}
            </div>
            <div>
              <strong>Amount Outstanding:</strong> {outstandingAmount}
            </div>
            <div>
              <strong>Invoice Date:</strong>{` `}
              <span style={{ color: 'red', fontWeight: 'bold' }}>
                {invoice?.invoiceDate?.toString()?.split('T')?.[0]}
              </span>
            </div>
          </Col>
          <Col>
            <Card>
              <Card.Body style={{ padding: '.75rem' }}>
                {invoiceAddress}
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="align-items-end">
          <Col style={{ textAlign: 'right' }}>
          </Col>
        </Row>
        <Table striped bordered hover variant="dark" size="sm">
          <thead>
            <tr>
              <th>Item</th>
              <th>Description</th>
              <th>Unit Cost</th>
              <th>Quantity</th>
              <th>Line Total</th>
            </tr>
          </thead>
          <tbody>
            {(lineData || []).map((line, i) => (
              <InvoiceLine
                key={i}
                line={line}
              />
            ))}
          </tbody>
          <tfoot>
            <tr>
              <th colSpan={4}>Subtotal</th>
              <th className='currency-right'>{invoiceSubtotal}</th>
            </tr>
            <tr>
              <td colSpan={4}>Tax: ({invoiceTaxRate})</td>
              <td colSpan={1} className='currency-right'>{invoiceTax}</td>
            </tr>
            <tr>
              <th colSpan={4}>Total</th>
              <th colSpan={1} className='currency-right'>{invoiceTotal}</th>
            </tr>
            <tr>
              <th colSpan={4}>Paid</th>
              <th colSpan={1} className='currency-right'>{invoicePaidAmount}</th>
            </tr>
            <tr>
              <th colSpan={4}>Outstanding</th>
              <th colSpan={1} className='currency-right'>{outstandingAmount}</th>
            </tr>
          </tfoot>
        </Table>
      </Container>
    </>
  );
};

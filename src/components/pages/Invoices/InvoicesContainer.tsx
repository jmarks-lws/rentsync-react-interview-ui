import { Route, Switch } from "react-router-dom";

import { InvoicesListView } from "./InvoicesListView";
import { InvoiceSingleView } from './InvoiceSingleView';

export const InvoicesContainer = () => {
  return (
    <div>
      <Switch>
        <Route path="/invoices/:invoiceId"><InvoiceSingleView /></Route>
        <Route path="/invoices"><>
          <header className="Section-header">Invoices</header>
          <InvoicesListView />
        </></Route>
      </Switch>
    </div>
  )
}
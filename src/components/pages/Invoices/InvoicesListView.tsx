import { useEffect } from 'react';

import {
  FormCheck, Table, Container, Form, Col,
} from 'react-bootstrap';

import { useAppSelector } from '../../../redux/hooks';
import { loadInvoices } from '../../../redux/features/invoices/actions';

import { Link } from 'react-router-dom';

export const InvoicesListView = () => {
  const invoices = useAppSelector(state => state.invoices.invoices);

  const sortBy = (criteria: string) => { }
  const sortOrder = (criteria: string): null | 'ASC' | 'DESC' => { return null; }
  const getSortIcon = (criteria: string) => {
    const sort = sortOrder(criteria);
    return sort === null ? null : (sort === 'ASC' ? <>&#x25bc;</> : <>&#x25b2;</>);
  }

  useEffect(() => { loadInvoices(); }, [])

  return (
    <>
      <Container>
        <Form>
          <Form.Row>
            <Col xs={3}>
              <label htmlFor="clientNameSearch">Client</label>
            </Col>
            <Col xs={9}>
              <Form.Control id="clientNameSearch" type="text" placeholder="Enter part of the client name" />
            </Col>
          </Form.Row>

          <Form.Row>
            <Col xs={3}>
              <label htmlFor="invoiceNumberSearch">Invoice #:</label>
            </Col>
            <Col xs={9}>
              <Form.Control id="invoiceNumberSearch" type="text" placeholder="Enter part of the invoice number" />
            </Col>
          </Form.Row>
        </Form>
      </Container>

      <Container>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr style={{ cursor: 'pointer' }}>
              <td style={{ textAlign: 'center' }}>
                <FormCheck checked={false} onChange={() => {}} />
              </td>
              <td
                onClick={(e) => sortBy('invoiceNumber')}
                style={{ whiteSpace: 'nowrap' }}
              >Invoice # {getSortIcon('invoiceNumber')}</td>
              <td
                onClick={(e) => sortBy('clientName')}
                style={{ whiteSpace: 'nowrap' }}
              >Client {getSortIcon('clientName')}</td>
              <td
                onClick={(e) => sortBy('invoiceDate')}
              >Date {getSortIcon('invoiceDate')}</td>
              <td
                onClick={(e) => sortBy('amountInvoiced')}
                style={{ whiteSpace: 'nowrap' }}
              >Subtotal {getSortIcon('amountInvoiced')}</td>
              <td
                onClick={(e) => sortBy('status')}
                style={{ whiteSpace: 'nowrap' }}
              >Status {getSortIcon('status')}</td>
              <td>&nbsp;</td>
            </tr>
          </thead>
          <tbody>
            {(invoices || [])
              .map((i) => (
                <tr key={i.id}>
                  <td>&nbsp;</td>
                  <td>{i.invoiceNumber}</td>
                  <td>{i.client.name}</td>
                  <td>{i.dueDate}</td>
                  <td>?</td>
                  <td>{i.status}</td>
                  <td>
                    <Link to={`/invoices/${i.id}`}>View Invoice</Link>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </Container>
    </>
  );
}

import hydraLogo from '../../assets/logo_2.svg';
import { Link } from "react-router-dom";
import { Button, Container } from 'react-bootstrap';

export const Home = (props: Record<string, any>) => {
  return (
    <Container>
      <div className="App">
        <header className="App-header">
          <div>
            <h1>Rentsync Coding Interview App</h1>
          </div>
          <div>
            <Link to="/clients" style={{ fontWeight: 'bold' }}>
              <img src={hydraLogo} alt="logo" width="150px" height="150px" />
            </Link>
          </div>
        </header>
        <div className="App-intro">
          <h3>Welcome to Rentsync's React Coding Interview App</h3>
          <p>
            Hey There!
          </p>
          <p>
            If you're seeing this app, it means we think you might be a good fit at Rentsync. Thanks
            for taking the time to meet with us. We look forward to seeing you again soon.
          </p>
          <h4>About the Application</h4>
          <p>
            This bare bones application made with <code>React</code>, <code> redux-toolkit</code>,
            and <code>react-bootstrap</code> contains a very basic set up for a React application, with
            many missing pieces when it comes to business logic. Our very incomplete application is just waiting for
            you to apply your own personal signature to it.
          </p>
          <p>
            For our purposes, this application does not connect to a &quot;real&quot; API service. We simulate a
            back end with a simple JSON file. However, when working with this code, you may act as if the store
            were connected to a true REST API for which you have read-only access.
          </p>

          <h4>Interview Format</h4>
          <p>
            We really want to see who you are as a developer, so rather than throw an unfamiliar code base at you
            we ask that you please take the next day or two to familiarize yourself with this (admittedly limited)
            project. When you are ready, we'll see you again.
          </p>
          <p>
            During your interview, you'll be given the choice of several tickets which will range in difficulty.
            Types of tickets will range from simple bug fixes to somewhat complex feature enhancements.
          </p>
          <p>
            This format is meant to allow you to choose something that you feel you can best showcase your capabilities
            with. What you choose take on is up to you. You can tackle the easiest task, the most difficult, or
            something in between. If you want, you can even try to complete more than one. (But, one is usually enough.)
          </p>
          <p>
            You'll only have about 20-25 minutes, so choose wisely.
          </p>
          <h4>See you Soon!</h4>
          <p>
            We want you to be successful. Our goal is to see how you approach problem solving - Not to stump you.
          </p>
          <p>Here a few final pointers:</p>
          <ul>
            <li>
              You can ask us questions at any time for clarification or advice. Asking questions is encouraged.
            </li>
            <li>
              Referencing Google, Stackoverflow, or whatever you like to use online is cool with us. We all use these
              resources every day in our work. Heck, keep a few sites bookmarked if you think they'll be useful during
              the interview.
            </li>
            <li>
              Consider this to be a conversation. Nerves are normal, but we'll get a better idea of who you are as a
              developer if you can act closer to your every day.
            </li>
            <li>
              At the end of your interview, we will likely ask you a few questions like why you chose a particular
              task or why you chose to approach it the way you did.
            </li>
          </ul>
          <p>
            Thank you again for your application and your interest in joining us at Rentsync.
          </p>
        </div>
        <Button
          size="sm"
          color="primary"
          as={Link}
          to="/clients"
          style={{
            fontWeight: 'bold',
            position: 'fixed',
            top: '10px',
            right: '10px',
          }}
        >
          Continue to the Application
        </Button>
      </div>
    </Container>
  )
}
import { useEffect } from "react"
import { Route, Switch } from "react-router-dom"

import { useAppSelector } from "../../../redux/hooks"
import { loadClients } from "../../../redux/features/clients/actions"

import { IClient } from "../../../app-utils/database";

import { ClientsListView } from './ClientsListView'
import { ClientSingleView } from "./ClientSingleView";

export const ClientsContainer = () => {
  const clients: IClient[] = useAppSelector(state => state.clients.clients);

  useEffect(() => { loadClients(); }, []);

  return (
    <>
      <header className="Section-header">Clients</header>

      <Switch>
        <Route path="/clients/:clientId"><ClientSingleView /></Route>
        <Route path="/clients"><ClientsListView clients={(clients || [])} /></Route>
      </Switch>
    </>
  )
}

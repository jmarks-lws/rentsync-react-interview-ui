import { Nullable } from "@jamesgmarks/utilities";
import { Container, Table } from 'react-bootstrap';
import { Link } from "react-router-dom";

import { IClient } from "../../../app-utils/database";

const ClientsListDataRow = ({ client, setSelectedClient }: {
  client: IClient,
  setSelectedClient: (client: Nullable<IClient>) => void,
}) => {
  return (
    <tr>
      <td>{client.id}</td>
      <td>{client.name}</td>
      <td>{client.email}</td>
      <td>{client.city}, {client.province}</td>
      <td>
        <Link to={`/clients/${client.id}`}>View Client</Link>
      </td>
    </tr>
  );
}

export const ClientsListView = ({ clients }: { clients: IClient[] }) => {
  return (<>
    <Container>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>ID </th>
            <th>Name</th>
            <th>Email</th>
            <th>Location</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {(clients || []).map(client => (
            <ClientsListDataRow
              key={client.id}
              client={client}
              setSelectedClient={()=>{}}
            />
          ))}
        </tbody>
      </Table>
    </Container>
  </>)
}
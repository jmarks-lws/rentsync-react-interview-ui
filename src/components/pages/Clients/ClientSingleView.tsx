import { intVal } from '@jamesgmarks/utilities';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Col, Container, Row } from 'react-bootstrap';

import { useAppSelector } from '../../../redux/hooks';
import { setCurrentClient } from '../../../redux/features/clients/actions';

export const ClientSingleView = () => {
  const { clientId } = useParams<{ clientId?: string }>();

  const client = useAppSelector(state => state.clients.currentClient);

  useEffect(() => { setCurrentClient(intVal(clientId ?? '0')); }, [ clientId ]);

  if (!client) return <>Invalid Client</>;

  return (
    <>
      <Container>
        <Row className="align-items-start">
          <Col>
            <div>
              <h4>{client.name}</h4>
              <strong>Client #: {clientId} </strong>
            </div>
            <div>
              <strong>Address: </strong>
              <div>
                <div>{client.address}</div>
                <div>{client.city}, {client.province}, {client.country}</div>
                <div>{client.postal}</div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}
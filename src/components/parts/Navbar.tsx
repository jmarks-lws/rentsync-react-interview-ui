import navLogo from '../../assets/nav_logo.svg';
import { Link, useLocation } from "react-router-dom";
import { Navbar as BsNavbar, Nav } from 'react-bootstrap';

export const UserNav = () => {
  const userNavMap = [
    { label: 'Clients', toLink: '/clients' },
    { label: 'Invoices', toLink: '/invoices' },
  ];
  return (<Nav>
    {userNavMap.map((item, i) => (
      <Nav.Link key={i} as={Link} to={item.toLink}>{item.label}</Nav.Link>
    ))}
  </Nav>);
}

export const Navbar = (props: Record<string, any>) => {
  const location = useLocation();

  return (
    (location?.pathname === '/')
      ? <div />
      : <BsNavbar bg="dark" variant="dark">
        <BsNavbar.Brand href="/">
          <img src={navLogo} className="App-logo" alt="logo" style={{height: '30px'}} />
        </BsNavbar.Brand>
        <Nav>
          {props.isLoggedIn && <UserNav />}
        </Nav>
        <Nav className="mr-auto"></Nav>
        <Nav>
          <Nav.Link onClick={() => window.history.back()}>Back</Nav.Link>
        </Nav>
      </BsNavbar>
  )
};